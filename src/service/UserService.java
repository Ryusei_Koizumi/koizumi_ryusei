package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	/**
	 * 新規ユーザ登録
	 *
	 * @param user	入力されたユーザ情報
	 */
	public void insert(User user) throws IOException, ServletException{

		Connection connection = null;
		try {
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection, user);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * ログインのため、フォームで送信された情報をDBと照合する
	 *
	 * @param account login.jspから渡されたアカウント名
	 * @param password	login.jspから渡されたパスワード
	 * @return	該当のユーザー情報
	 */
	public User select(String account, String password) {

		Connection connection = null;
		try {
			String encPassword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().select(connection, account, encPassword);
			commit(connection);
			return user;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * management.jspで表示する情報を取得する
	 *
	 * @return 支社/部署を含めたユーザ情報
	 */
	public List<UserBranchDepartment> select(){

		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserBranchDepartment> userBranchDepartments = new UserBranchDepartmentDao().select(connection, LIMIT_NUM);
			commit(connection);
			return userBranchDepartments;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * ユーザ設定画面で表示する情報の取得
	 *
	 * @param id	編集するユーザのid
	 * @return		user情報（beans参照）
	 */
	public User select(int id) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, id);
			commit(connection);
			return user;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * ユーザ情報を設定画面から更新する
	 *
	 * @param user		設定画面から送信された情報
	 */
	public void update(User user) {

		Connection connection = null;
		try {
			if( !StringUtils.isEmpty(user.getPassword()) ) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}
			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * ユーザの復活停止ステータスを変更する
	 *
	 * @param id	対象のユーザ
	 * @param stopOrRevive	復活/停止の番号
	 */
	public void stopOrRevive(int id, int stopOrRevive) {

		Connection connection = null;
		try {
			connection = getConnection();
			new UserDao().stopOrRevive(connection, id, stopOrRevive);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * 重複ユーザの照合
	 *
	 * @param account	入力されたアカウント名
	 * @return		同一アカウント名のユーザ情報（なければnull）
	 */
	public User select(String account) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, account);
			commit(connection);
			return user;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}