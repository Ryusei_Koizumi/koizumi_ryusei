package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		int userId = Integer.parseInt(request.getParameter("user-id"));
		new UserService().stopOrRevive(userId, Integer.parseInt(request.getParameter("user-stop")));

		response.sendRedirect("management");
	}
}