package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet("/message")
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();
		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));
		if(!isValid(message, errorMessages)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		User user = (User)session.getAttribute("loginUser");
		message.setUserId(user.getId());

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String messageTitle = message.getTitle();
		String messageCategory = message.getCategory();
		String messageText = message.getText();
		int messageTitleMax = 30;
		int messageCategoryMax = 10;
		int messageTextMax = 1000;

		if (StringUtils.isBlank(messageTitle)) {
			errorMessages.add("件名を入力してください");
		} else if (messageTitle.length() > messageTitleMax) {
			errorMessages.add("件名は30字以内で入力してください");
		}
		if (StringUtils.isBlank(messageCategory)) {
			errorMessages.add("カテゴリーを入力してください");
		} else if (messageCategory.length() > messageCategoryMax) {
			errorMessages.add("カテゴリーは10字以内で入力してください");
		}
		if (StringUtils.isBlank(messageText)) {
			errorMessages.add("本文を入力してください");
		} else if (messageText.length() > messageTextMax) {
			errorMessages.add("本文は1000字以内で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}