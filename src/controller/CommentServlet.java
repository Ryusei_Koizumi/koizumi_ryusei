package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();
		Comment comment = new Comment();
		comment.setText(request.getParameter("comment-text"));
		comment.setMessageId(Integer.parseInt(request.getParameter("message-id")));
		if (!isValid(comment, errorMessages)) {
			session.setAttribute("writedComment", comment);
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		User user = (User)session.getAttribute("loginUser");
		comment.setUserId(user.getId());

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private boolean isValid(Comment comment, List<String> errorMessages) {

		String commentText = comment.getText();
		int commentMax = 500;

		if (StringUtils.isBlank(commentText)) {
			errorMessages.add("コメントを入力してください");
		} else if (commentMax < commentText.length()) {
			errorMessages.add("コメントは500字以内で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}