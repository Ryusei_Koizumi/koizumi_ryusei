package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		//投稿絞り込み時の入力値の保持
		String startDate = request.getParameter("start-date");
		String endDate = request.getParameter("end-date");
		String category = request.getParameter("category");
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("category", category);

		//投稿絞り込み
		List<UserMessage> messages;
		messages = new MessageService().select(startDate, endDate, category);
		request.setAttribute("messages", messages);

		List<UserComment> comments = new CommentService().select();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}
}