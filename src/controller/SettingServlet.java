package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet("/setting")
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();
		String alphaNumericRegex = "^[0-9]{1,}$";
		User user = null;

		if (!StringUtils.isEmpty(request.getParameter("user-branch-department-id")) &&
				request.getParameter("user-branch-department-id").matches(alphaNumericRegex)) {

			int id = Integer.parseInt(request.getParameter("user-branch-department-id"));
			user = new UserService().select(id);

			//↓ここから
			if (user == null) {
				errorMessages.add("不正なパラメータが確認されました");
				request.setAttribute("errorMessages", errorMessages);
				request.getRequestDispatcher("/management").forward(request, response);
				return;
			}
		} else {
			errorMessages.add("不正なパラメータが確認されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("/management").forward(request, response);
			return;
		}
		//↑ここまでが１つまとまる

		request.setAttribute("user", user);

		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);

		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();

		//エラー時の値の保持
		String selectedBranchId = request.getParameter("branch-id");
		String selectedDepartmentId = request.getParameter("department-id");
		request.setAttribute("selectedBranchId", selectedBranchId);
		request.setAttribute("selectedDepartmentId", selectedDepartmentId);

		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);

		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		User user = getUser(request);
		if (!isValid(request, user, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}
		new UserService().update(user);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException{

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch-id")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department-id")));

		return user;
	}

	private boolean isValid(HttpServletRequest request, User user, List<String> errorMessages) {

		int loginId = user.getId();
        String account = user.getAccount();
        String password = user.getPassword();
        String checkPassword = request.getParameter("check-password");
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        String alphabetNumberRegex = "^[A-Za-z0-9]{6,20}+$";
        int passMax = 20;
        int passMin = 6;
        int nameMax = 10;
        int headBranch = 1;
        int personnelDepartment = 1;
        int informationDepartment = 2;
        int salesDepartment = 3;
        int engineerDepartment = 4;

        User overlapUser = new UserService().select(account);
        if( overlapUser != null && loginId != overlapUser.getId() ) {
			errorMessages.add("アカウントが重複しています");
		}

        if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (!account.matches(alphabetNumberRegex)) {
			errorMessages.add("アカウントは6字～20字の半角英数字で入力してください");
		}

		if (!StringUtils.isBlank(password) && ((passMax < password.length()) || (password.length() < passMin))) {
			errorMessages.add("パスワードは6字～20字で入力してください");
		}
    	if (!password.equals(checkPassword)) {
    		errorMessages.add("パスワードと確認用パスワードが一致していません");
    	}

    	if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (nameMax < name.length()) {
			errorMessages.add("名前は10文字以内で入力してください");
		}

		if ((branchId == headBranch) &&
				(departmentId == salesDepartment || departmentId == engineerDepartment)) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		} else if ((branchId != headBranch) &&
				(departmentId == informationDepartment || departmentId == personnelDepartment)) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}