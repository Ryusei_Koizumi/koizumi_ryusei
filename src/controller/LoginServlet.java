package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet{


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		int stopped = 1;

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		}
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		User user = new UserService().select(account, password);
		if ((!StringUtils.isEmpty(account) && !StringUtils.isEmpty(password)) &&
				((user == null) || (user.getIsStopped() == stopped))) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("account", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");

	}
}