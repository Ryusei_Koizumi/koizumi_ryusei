package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection, int LIMIT_NUM) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	users.id as id, ");
			sql.append("	users.account as account, ");
			sql.append("	users.name as name, ");
			sql.append("	branches.name as branch, ");
			sql.append("	departments.name as department, ");
			sql.append("	users.is_stopped as isStopped ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.id DESC limit " + LIMIT_NUM);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> users = toUsers(rs);
			return users;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUsers(ResultSet rs) throws SQLException{

		List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
		try {
			while(rs.next()) {
				UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
				userBranchDepartment.setId(rs.getInt("id"));
				userBranchDepartment.setAccount(rs.getString("account"));
				userBranchDepartment.setName(rs.getString("name"));
				userBranchDepartment.setBranch(rs.getString("branch"));
				userBranchDepartment.setDepartment(rs.getString("department"));
				userBranchDepartment.setIsStopped(rs.getInt("isStopped"));
				if (rs.getInt("isStopped") == 0) {
					userBranchDepartment.setIsStoppedName("稼働中");
				} else {
					userBranchDepartment.setIsStoppedName("停止中");
				}

				users.add(userBranchDepartment);
			}
			return users;
		} finally {
			close(rs);
		}
	}
}