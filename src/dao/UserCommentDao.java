package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection, int LIMIT_NUM) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	comments.id as id, ");
			sql.append("	users.name as name, ");
			sql.append("	comments.user_id as commentUserId, ");
			sql.append("	comments.message_id as messageId, ");
			sql.append("	comments.text as text, ");
			sql.append("	comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY comments.created_date ASC limit " + LIMIT_NUM);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> comments = toUserComments(rs);
			return comments;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	private List<UserComment> toUserComments(ResultSet rs) throws SQLException{

		List<UserComment> comments = new ArrayList<UserComment>();
		try {
			while(rs.next()) {
				UserComment comment = new UserComment();
				comment.setId(rs.getInt("id"));
				comment.setName(rs.getString("name"));
				comment.setUserId(rs.getInt("commentUserId"));
				comment.setMessageId(rs.getInt("messageId"));
				comment.setText(rs.getString("text"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));

				comments.add(comment);
			}
			return comments;
		} finally {
			close(rs);
		}
	}
}