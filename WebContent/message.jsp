<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<img src="./LOGO.PNG" class="logo">
				<div class="menu">
					<a href="./">ホーム</a>
				</div>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div>
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<form action="message" method="post">

				<label for="title">件名</label><br/>
				<input name="title" id="messagetitle" value="${message.title}"  placeholder="30字以内で入力してください"/><br />

				<label for="category">カテゴリー</label><br/>
				<input name="category" id="messagecategory" value="${message.category}" placeholder="10字以内で入力してください"/><br />

				<label for="text">本文</label><br/>
				<textarea name="text" id="messagetext" rows="5" placeholder="1000字以内で入力してください">${message.text}</textarea>

				<br />
				<input type="submit" value="投稿" />
			</form>

			<div class="copyright">Copyright&copy; RyuseiKoizumi</div>
		</div>
	</body>
</html>