<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<div class="menu">
					<a href="management">ユーザー管理</a><br />
				</div>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="signup" method="post">

				<label for="account">アカウント</label>
				<input name="account" id="account" value="${user.account}" placeholder="6字以上20字以下の半角英数字"/><br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password" placeholder="6字以上20字以下の半角記号文字"/> <br />

				<label for="check-password">確認用パスワード</label>
				<input name="check-password" type="password" id="check-password" placeholder="6字以上20字以下の半角記号文字"/> <br />

				<label for="name">名前</label>
				<input name="name" id="name" value="${user.name}" placeholder="1字以上10字以下の文字"/><br />

				<c:if test="${empty errorMessages}">
					<div class="branch">
						<label for="branch">支社</label>
						<select name="branch-id">
							<c:forEach items="${branches}" var="branch">
								<option value="${branch.id}"><c:out value="${branch.name}" /></option>
							</c:forEach>
						</select>
					</div>
					<div class="department">
						<label for="department">部署</label>
						<select name="department-id">
							<c:forEach items="${departments}" var="department">
								<option value="${department.id}"><c:out value="${department.name}" /></option>
							</c:forEach>
						</select>
					</div>
				</c:if>

				<c:if test="${not empty errorMessages}">
					<label for="branch">支社</label>
					<select name="branch-id">
						<c:if test="${loginUser.id == user.id}">
							<c:forEach items="${branches}" var="branch">
								<c:if test="${branch.id == selectedBranchId}"><option value="${branch.id}" selected><c:out value="${branch.name}"/></option></c:if>
								<c:if test="${branch.id != selectedBranchId}"><option value="${branch.id}" disabled><c:out value="${branch.name}"/></option></c:if>
							</c:forEach>
						</c:if>
						<c:if test="${loginUser.id != user.id}">
							<c:forEach items="${branches}" var="branch">
								<c:if test="${branch.id == selectedBranchId || branch.id == selectedBranchId}"><option value="${branch.id}" selected><c:out value="${branch.name}"/></option></c:if>
								<c:if test="${branch.id != selectedBranchId}"><option value="${branch.id}" ><c:out value="${branch.name}"/></option></c:if>
							</c:forEach>
						</c:if>
					</select>
					<br />

					<label for="department">部署</label>
					<select name="department-id">
						<c:if test="${loginUser.id == user.id}">
							<c:forEach items="${departments}" var="department">
								<c:if test="${department.id == selectedDepartmentId}"><option value="${department.id}" selected><c:out value="${department.name}"/></option></c:if>
								<c:if test="${department.id != selectedDepartmentId}"><option value="${department.id}" disabled><c:out value="${department.name}"/></option></c:if>
							</c:forEach>
						</c:if>
						<c:if test="${loginUser.id != user.id}">
							<c:forEach items="${departments}" var="department">
								<c:if test="${department.id == selectedDepartmentId}"><option value="${department.id}" selected><c:out value="${department.name}"/></option></c:if>
								<c:if test="${department.id != selectedDepartmentId}"><option value="${department.id}"><c:out value="${department.name}"/></option></c:if>
							</c:forEach>
						</c:if>
					</select>
					<br />
				</c:if>

				<input type="submit" value="登録" /> <br />

			</form>

			<div class="copyright">Copyright&copy; RyuseiKoizumi</div>
		</div>
	</body>
</html>