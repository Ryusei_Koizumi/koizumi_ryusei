<%@page import="beans.Message"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<div class="logo">
					<img src="./LOGO.PNG" alt="logo" class="logo">
				</div>
				<div class="menu">
					<a href="message">新規投稿</a>
					<c:if test="${loginUser.branchId == 1 && loginUser.departmentId == 1}">
						<a href="management">ユーザー管理</a>
					</c:if>
					<a href="logout">ログアウト</a>
				</div>

				<div class="filter">
					<form action="./" method="get">
						<div class="date">
							<label for="start-date">開始日</label>
							<input type="date" name="start-date" id="start-date" value="${startDate}"/>終了日
							<input type="date" name="end-date" id="end-date" value="${endDate}"/>
						</div>

						<div class="category">
							<label for="category">カテゴリ</label>
							<input name="category" id="category" value="${category}" placeholder="10字以下で入力してください"/>
							<input type="submit" value="絞り込む" />
						</div>
					</form>
				</div>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="contents">
				<c:forEach items="${messages}" var="message" varStatus="current">
					<div class="message">
						<span class="name">投稿者：<c:out value="${message.name}" /></span>
						<span class="title">件名：<c:out value="${message.title}" /></span>
						<span class="category">カテゴリー：<c:out value="${message.category}" /></span><br />
						<div class="text">
							<pre><c:out value="${message.text}" /></pre>
					    </div>
						<div class="created-date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<c:if test="${loginUser.id == message.userId}">
							<form action="deleteMessage" method="post">
								<!-- 確認ダイアログ出力JS -->
								<script type="text/javascript">
									function submitcheck() {
										var check = confirm('投稿を削除しますか？');
										return check;
									}
								</script>
								<input name="message-id" value="${message.id}" id="id" type="hidden"/>
								<input type="submit" name ="submit" value="削除" onclick="return submitcheck();">
							</form>
						</c:if>
					</div>

					<div class="comment">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId}">
								<span class="commentname">コメント：<c:out value="${comment.name}" /></span>
								<div class="commenttext">
									<pre><c:out value="${comment.text}" /></pre>
					    		</div>
								<div class="commentdate">
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<c:if test="${loginUser.id == comment.userId}">
									<form action="deleteComment" method="post">
										<!-- 確認ダイアログ出力JS -->
										<script type="text/javascript">
											function submitcommentcheck() {
												var check = confirm('コメントを削除しますか？');
												return check;
											}
										</script>
										<input name="comment-id" value="${comment.id}" id="id" type="hidden"/>
										<input type="submit" value="削除" onclick="return submitcommentcheck();">
									</form>
								</c:if>
							</c:if>
						</c:forEach>
					</div>

					<div class="comment-form-area">
						<form action="comment" method="post">
							<input name="message-id" value="${message.id}" id="id" type="hidden" />
							コメント入力欄<br />
							<textarea class="comment-text" name="comment-text" cols="30" rows="3" placeholder="500字以下で入力してください"></textarea>
							<br />
							<input type="submit" value="コメント投稿">
						</form>
						<br />
					</div>
				</c:forEach>
			</div>

			<div class="copyright">Copyright&copy; RyuseiKoizumi</div>

		</div>
	</body>
</html>