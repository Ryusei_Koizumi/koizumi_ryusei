<%@page import="beans.Message"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<img src="./LOGO.PNG" class="logo">
				<div class="menu">
					<a href="./">ホーム</a>
					<a href="signup">ユーザー新規登録</a><br />
				</div>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="contents">
				<c:forEach items="${userBranchDepartments}" var="userBranchDepartment">
					<div class="card">
						<div class="account">アカウント：<c:out value="${userBranchDepartment.account}" /></div>
						<div class="name">名前：<c:out value="${userBranchDepartment.name}" /></div>
						<div class="branch">支社：<c:out value="${userBranchDepartment.branch}" /></div>
						<div class="department">部署：<c:out value="${userBranchDepartment.department}" /></div>
						<div class="isStopped">ステータス：<c:out value="${userBranchDepartment.isStoppedName}" /></div>

							<form action="setting" method="get">
								<input name="user-branch-department-id" value="${userBranchDepartment.id}" id="user-branch-department-id" type="hidden"/>
								<input type="submit" id="setting-button" value="編集">
							</form>

							<form action="stop" method="post">
								<input name="user-id" value="${userBranchDepartment.id}" id="id" type="hidden"/>
								<c:if test="${userBranchDepartment.isStopped == 0}">
									<!-- 確認ダイアログ出力JS -->
									<script type="text/javascript">
										function submitcheck() {
											var check = confirm('停止させますか？');
											return check;
										}
									</script>
									<c:if test="${loginUser.id == userBranchDepartment.id}">
										<input name="user-stop" value="1" id="is-stop" type="hidden"/>
										<input type="submit" value="停止" id="stop-button" onclick="return submitcheck();" disabled/>
									</c:if>
									<c:if test="${loginUser.id != userBranchDepartment.id}">
										<input name="user-stop" value="1" id="is-stop" type="hidden"/>
										<input type="submit" value="停止" id="stop-button" onclick="return submitcheck();"/>
									</c:if>
								</c:if>
								<c:if test="${userBranchDepartment.isStopped == 1}">
									<!-- 確認ダイアログ出力JS -->
									<script type="text/javascript">
										function submitcheck() {
											var check = confirm('復活させますか？');
											return check;
										}
									</script>
									<input name="user-stop" value="0" id="is-stop" type="hidden"/>
									<input type="submit" value="復活" id="stop-button"  onclick="return submitcheck();"/>
								</c:if>
							</form>
					</div>
				</c:forEach>
			</div>

			<div class="copyright">Copyright&copy; RyuseiKoizumi</div>

		</div>
	</body>
</html>