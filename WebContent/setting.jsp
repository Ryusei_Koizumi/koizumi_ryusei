<%@page import="beans.Message"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
			<img src="./LOGO.PNG" class="logo">
				<div class="menu">
					<a href="management">ユーザー管理</a>
				</div>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="contents">
				<form action="setting" method="post">
					<input name="id" value="${user.id}" id="setting-id" type="hidden" />

					<label for="account">アカウント</label><br/>
					<input name="account" id="setting-account" value="${user.account}" placeholder="6字以上20字以下の半角英数字"/><br />

					<label for="password">パスワード</label><br/>
					<input name="password" type="password" id="setting-password" placeholder="6字以上～20字以下の半角記号文字"/><br />

					<label for="check-password">確認用パスワード</label><br/>
					<input name="check-password" type="password" id="check-password" placeholder="6字以上～20字以下の半角記号文字"/><br />

					<label for="name">名前</label><br/>
					<input name="name" id="setting-name" value="${user.name}" placeholder="1字以上10字以下の文字"/><br />

					<!-- エラー表示時の値の保持機能 -->
					<c:if test="${not empty errorMessages}">
						<label for="branch">支社</label><br />
						<select name="branch-id" id="branch">
							<c:if test="${loginUser.id == user.id}">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${branch.id == selectedBranchId}"><option value="${branch.id}" selected><c:out value="${branch.name}"/></option></c:if>
									<c:if test="${branch.id != selectedBranchId}"><option value="${branch.id}" disabled><c:out value="${branch.name}"/></option></c:if>
								</c:forEach>
							</c:if>
							<c:if test="${loginUser.id != user.id}">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${branch.id == selectedBranchId || branch.id == selectedBranchId}"><option value="${branch.id}" selected><c:out value="${branch.name}"/></option></c:if>
									<c:if test="${branch.id != selectedBranchId}"><option value="${branch.id}" ><c:out value="${branch.name}"/></option></c:if>
								</c:forEach>
							</c:if>
						</select>
						<br />

						<label for="department">部署</label><br />
						<select name="department-id" id="department">
							<c:if test="${loginUser.id == user.id}">
								<c:forEach items="${departments}" var="department">
									<c:if test="${department.id == selectedDepartmentId}"><option value="${department.id}" selected><c:out value="${department.name}"/></option></c:if>
									<c:if test="${department.id != selectedDepartmentId}"><option value="${department.id}" disabled><c:out value="${department.name}"/></option></c:if>
								</c:forEach>
							</c:if>
							<c:if test="${loginUser.id != user.id}">
								<c:forEach items="${departments}" var="department">
									<c:if test="${department.id == selectedDepartmentId}"><option value="${department.id}" selected><c:out value="${department.name}"/></option></c:if>
									<c:if test="${department.id != selectedDepartmentId}"><option value="${department.id}"><c:out value="${department.name}"/></option></c:if>
								</c:forEach>
							</c:if>
						</select>
					</c:if>

					<c:if test="${empty errorMessages}">
						<label for="branch">支社</label><br />
						<select name="branch-id">
							<c:if test="${loginUser.id == user.id}">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${branch.id == user.branchId}"><option value="${branch.id}" selected><c:out value="${branch.name}"/></option></c:if>
									<c:if test="${branch.id != user.branchId}"><option value="${branch.id}" disabled><c:out value="${branch.name}"/></option></c:if>
								</c:forEach>
							</c:if>
							<c:if test="${loginUser.id != user.id}">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${branch.id == user.branchId || branch.id == selectedBranchId}"><option value="${branch.id}" selected><c:out value="${branch.name}"/></option></c:if>
									<c:if test="${branch.id != user.branchId}"><option value="${branch.id}" ><c:out value="${branch.name}"/></option></c:if>
								</c:forEach>
							</c:if>
						</select>
						<br />

						<label for="department">部署</label><br />
						<select name="department-id">
							<c:if test="${loginUser.id == user.id}">
								<c:forEach items="${departments}" var="department">
									<c:if test="${department.id == user.departmentId}"><option value="${department.id}" selected><c:out value="${department.name}"/></option></c:if>
									<c:if test="${department.id != user.departmentId}"><option value="${department.id}" disabled><c:out value="${department.name}"/></option></c:if>
								</c:forEach>
							</c:if>
							<c:if test="${loginUser.id != user.id}">
								<c:forEach items="${departments}" var="department">
									<c:if test="${department.id == user.departmentId || department.id == selectedDepartmentId}"><option value="${department.id}" selected><c:out value="${department.name}"/></option></c:if>
									<c:if test="${department.id != user.departmentId}"><option value="${department.id}"><c:out value="${department.name}"/></option></c:if>
								</c:forEach>
							</c:if>
						</select>
					</c:if>
					<br />

					<input type="submit" value="更新" /><br />
				</form>
			</div>

			<div class="copyright">Copyright&copy; RyuseiKoizumi</div>
		</div>
	</body>
</html>